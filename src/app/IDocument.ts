export interface IDocument {
    documentId: string;
    namespace: string;
    readOnly: boolean;
    content: string;
}