import { IDocument } from './../IDocument';
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  largeDocumentTest = {"page":3,"per_page":3,"total":12,"total_pages":4,"data":[{"id":7,"first_name":"Michael","last_name":"Lawson","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg"},{"id":8,"first_name":"Lindsay","last_name":"Ferguson","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg"},{"id":9,"first_name":"Tobias","last_name":"Funke","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg"}]};

  checkedOutDocuments:Array<IDocument> = [
    {
      documentId: 'Document 1',
      namespace: 'anamespace',
      readOnly: true,
      content: JSON.stringify({ foo: "sample", bar: "sample" }, null, 4)
    },
    {
      documentId: 'Document 2',
      namespace: 'anothernamespace',
      readOnly: true,
      content: JSON.stringify({ boo: "example", far: "example" }, null, 4)
    },
    {
      documentId: 'Document 3',
      namespace: 'yetanothernamespace',
      readOnly: true,
      content: JSON.stringify(this.largeDocumentTest, null, 4)
    }
  ];

  constructor(public navCtrl: NavController) {}

  onDocumentFormSubmitted($event) {
    //1) Add to Local Request Storage Queue
    //2) Eventually update checked out document list when PouchDb re-synchronises

    this.checkedOutDocuments.push({
      documentId: `Document ${this.checkedOutDocuments.length+1}`,
      namespace: $event.namespace,
      readOnly: true,
      content: JSON.stringify({ foo: "bizzle", bar: "shizzle" }, null, 4)
    });
  }
}
