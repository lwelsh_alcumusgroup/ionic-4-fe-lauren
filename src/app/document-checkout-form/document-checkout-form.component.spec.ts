import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentCheckoutFormComponent } from './document-checkout-form.component';

describe('DocumentCheckoutFormComponent', () => {
  let component: DocumentCheckoutFormComponent;
  let fixture: ComponentFixture<DocumentCheckoutFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentCheckoutFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentCheckoutFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
