import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-document-checkout-form',
  templateUrl: './document-checkout-form.component.html',
  styleUrls: ['./document-checkout-form.component.scss']
})
export class DocumentCheckoutFormComponent {
  public documentCheckoutForm: FormGroup;
  @Output() formSubmitEvent = new EventEmitter<string>();

  constructor(private formBuilder: FormBuilder) {
    this.documentCheckoutForm = this.formBuilder.group({
      namespace: ['', Validators.required],
      user: ['',  Validators.required],
      checkoutId: ['', Validators.required]
    });
  }

  emitDocumentSubmitEvent() {
    this.formSubmitEvent.emit(this.documentCheckoutForm.value);
    this.documentCheckoutForm.reset();
  }
}
