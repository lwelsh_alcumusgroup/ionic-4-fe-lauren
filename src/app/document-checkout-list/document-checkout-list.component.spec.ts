import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentCheckoutListComponent } from './document-checkout-list.component';

describe('DocumentCheckoutListComponent', () => {
  let component: DocumentCheckoutListComponent;
  let fixture: ComponentFixture<DocumentCheckoutListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentCheckoutListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentCheckoutListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
