import { IDocument } from './../IDocument';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-document-checkout-list',
  templateUrl: './document-checkout-list.component.html',
  styleUrls: ['./document-checkout-list.component.scss']
})
export class DocumentCheckoutListComponent {
  
  @Input() checkedOutDocuments: Array<IDocument>;

  constructor() {}
}
